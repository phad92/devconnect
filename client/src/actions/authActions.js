import axios from 'axios';
import jwt_decode from 'jwt-decode';

import setAuthToken from '../utils/setAuthToken';
import { GET_ERRORS, SET_CURRENT_USER } from './types';
// Register
export const registerUser = (userData, history) => dispatch => {
  axios.post('/api/users/register', userData)
      .then(res => history.push('/login'))
      .catch(err => 
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
        );
}

//Login - Get user token
export const loginUser = (userData) => dispatch => {
    axios.post('/api/users/login', userData)
        .then(res => {
            //save to localStorage
            const { token } = res.data;
            //set to localstorage
            localStorage.setItem('jwtToken', token);
            // Set token to Auth header
            setAuthToken(token);
            //Decode to token to get userdata
            const decodedToken = jwt_decode(token);
            //Set current user
            dispatch(setCurrentUser(decodedToken));
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
        })
}

//Set logged in user
export const setCurrentUser = decodedToken => {
    return {
        type: SET_CURRENT_USER,
        payload: decodedToken
    }
}

//log out user
export const logoutUser = () => dispatch => {
    //Remove token from localStorage
    localStorage.removeItem('jwtToken');
    //Remove auth header for future requests
    setAuthToken(false);

    //set current user to {} which will set isAuthenticated to false
    dispatch(setCurrentUser({}));
}